const express = require('express')
const bodyParser = require('body-parser')
const  request =require('request')

const APP_TOKEN = 'EAAZAoLRJJZB8IBAMZBd9nPG1jaPnZChi2kMHxeHdhnF6qDLawyRkIssxUrbMrXxLKqUJc0gHGJZBN4MA8J0tvtfJ1pthZAlFsmfufq7gZAuiNkoJkKRDO0GlD9EoZCuRJFmNtQmSN8RNEAqguZAnGie8VnopEM0SnSuKNZCI9Oxl4QMwZDZD'

var app = express()

app.use(bodyParser.json())

app.listen(3000, function(){
  console.log('Server listen localhost:3000')
})

app.get('/', function(req,res){
  res.send('Abriendo desde mi servidor localhost')
})
app.get('/webhook', function(req, res){
  if (req.query['hub.verify_token'] === 'hola_token') {
    res.send(req.query['hub.challenge'])
  }
  else {
    res.send('Usted no puede pasar')
  }
})

app.post('/webhook', function(req,res){
  var data =req.body
  if (data.object == 'page'){
    data.entry.forEach(function(pageEntry){
      pageEntry.messaging.forEach(function(messagingEvent){
        if (messagingEvent.message) {
          var senderID = messagingEvent.sender.id
          var messageText = messagingEvent.message.text

          var messageData = {
            recipient :{
              id:senderID
            },
            message:{
              text: 'Hola!, bienvenido a Chesta Records '+ messageText
            }
          }
          //api facebook
          request({
            uri: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {access_token: APP_TOKEN},
            method: 'POST',
            json: messageData
          },function(error, response, data){
            if (error) {
              console.log('No es posible enviar el mensaje')
            }
            else {
              console.log('Mensaje no enviado')
            }
          })
        }
      })
    })
  }
  console.log(data)
  res.sendStatus(200)
})
